# Nonogram

To read in other languages, it's here : [Français], [English], [Deutsch]

## What is it ?

Nonogram is a game. The principle of this game is to draw an image using the numbers in the margin.

### Rules

1. A number indicate the number of neighbouring cases
2. There is at least a blank between two series of black cases (one number)

## Features

### Completed:

### In Progress:

### Not yet started:

- Menu
- The Game
    - Prompting the image
    - Prompting the error
    - Stop on victory
    - Time increment
- Using a pre-made library of images
- Converting an image into a game-able file

## How to use it ?

If you want to compile this project, you should have :
- bash
- cmake
- make
- g++
- [CImg]

You should find the following files and run them :
- [use_cimg]
- [construct]


[CImg]: https://cimg.eu
[use_cimg]: ./The_Game/src/use_cimg
[construct]: ./The_Game/src/construct
[Français]: ./README.fr.md
[English]: ./README.md
[Deutsch]: ./README.de.md
